package com.sourcey.appcaptone.Controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.view.ContextMenu;
import android.view.MenuItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.Student;
import com.sourcey.appcaptone.Model.Subject_Class;
import com.sourcey.appcaptone.R;

import java.util.ArrayList;

import butterknife.BindView;

public class SearchMainActivity extends AppCompatActivity  implements View.OnTouchListener{
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    private DatabaseReference mDatabase;
    ArrayList<String> listLop = new ArrayList<String>();
    private static final int MENU_ITEM_VIEWHISTORY = 111;
    private static final int MENU_ITEM_VIEWSITUATION = 222;
    private static final int MENU_ITEM_ATENDANCE = 333;

    ArrayList<Subject_Class> sub_clasList = new ArrayList<>();
    Adapterlop_SearchSubjectClass adapterSearchSubClass;
    ListView listView;
    Button btnseach;
    String idUser;
    EditText editTextNamHoc, editTextHK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_main);
        editTextNamHoc = (EditText) findViewById(R.id.editNamhoc);
        editTextHK = (EditText) findViewById(R.id.editHK);
        Intent intent1 = getIntent();
        idUser = intent1.getExtras().getString("IDUS").toString();
        btnseach =(Button) findViewById(R.id.btnSearch);
        listView = (ListView) findViewById(R.id.listlop);


        btnseach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sub_clasList.removeAll(sub_clasList);
                getList();
                final ProgressDialog progressDialog = new ProgressDialog(SearchMainActivity.this,
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Loading !!!");
                progressDialog.show();

                // TODO: Implement your own authentication logic here.

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                // On complete call either onLoginSuccess or onLoginFailed

                                // onLoginFailed();
                                progressDialog.dismiss();
                            }
                        }, 1500);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent truyen = new Intent(SearchMainActivity.this, AttendenActivity.class);
                truyen.putExtra("MALOP","MACLENIN1_BIS1");

                //    truyen.putExtra("NAME","23232323");
                //   truyen.putExtra("URL","2323323232");
                startActivity(truyen);
            }
        });

    }


    void onSeltect(){
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
//                String maLop = listView.getItemAtPosition(i).toString();
//                Intent truyen = new Intent(SearchMainActivity.this, ViewHistoryMainActivity.class);
//                truyen.putExtra("MALOP",maLop);
//                startActivity(truyen);
//                             //  Toast.makeText(SearchMainActivity.this, maLop,Toast.LENGTH_LONG).show();
//            }
//        });
    }
    void getList(){

        getLop();
        adapterSearchSubClass = new Adapterlop_SearchSubjectClass(this, R.layout.custom_seachsubclass, sub_clasList);

        listView.setAdapter(adapterSearchSubClass);
        listView.setDivider(new ColorDrawable(Color.parseColor("#E5E5E5")));
        listView.setDividerHeight(2);
        //Hien Thi Khi tab tren dong
        registerForContextMenu(this.listView);



        onSeltect();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view,
                                    ContextMenu.ContextMenuInfo menuInfo)    {

        super.onCreateContextMenu(menu, view, menuInfo);
      //  menu.setHeaderTitle("What is your choose?")
        menu.add(0, MENU_ITEM_VIEWHISTORY , 0, "View Attendant History");
        menu.add(0, MENU_ITEM_VIEWSITUATION , 1, "View Situation");
        menu.add(0, MENU_ITEM_ATENDANCE , 1, "Attendacing Today");

    }
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
    final Subject_Class sub = (Subject_Class) this.listView.getItemAtPosition(info.position);

        if(item.getItemId() == MENU_ITEM_VIEWHISTORY){
            String maLop = sub.getClass_ID();
            Intent truyen = new Intent(SearchMainActivity.this, ViewHistoryMainActivity.class);
            truyen.putExtra("MALOP",maLop);
            startActivity(truyen);
            Toast.makeText(getApplicationContext(),maLop.toString(),Toast.LENGTH_LONG).show();
        }
        else if(item.getItemId() == MENU_ITEM_VIEWSITUATION){
            String maLop = sub.getClass_ID();
            Intent truyen1 = new Intent(SearchMainActivity.this, ViewSituationMainActivity.class);
            truyen1.putExtra("MALOP",maLop);
            startActivity(truyen1);
        }
        else if(item.getItemId() == MENU_ITEM_ATENDANCE){
            String maLop = sub.getClass_ID();
            Intent truyen1 = new Intent(SearchMainActivity.this, AttendenActivity.class);
            truyen1.putExtra("MALOP",maLop);
            startActivity(truyen1);
        }
        else {
            return false;
        }
        return true;
    }

    void getLop() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
//        myRef.child(editTextNamHoc.getText().toString()).child(editTextHK.getText().toString()).child(this.idUser).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
//                  //  listLop.add(childSnapshot.getKey().toString());
//                   // String classID = childSnapshot.getKey().toString();
//                    String subName = dataSnapshot.child("TENMON").getValue().toString();
//                    Log.d("KEY:",dataSnapshot.getKey().toString());
//                    Log.d("NAME:",subName);
//                    Subject_Class sc = new Subject_Class(childSnapshot.getKey().toString(),subName);
//                    sub_clasList.add(sc);
//
//                    adapterSearchSubClass.notifyDataSetChanged();
//                }
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
        try {
            myRef.child(editTextNamHoc.getText().toString()).child(editTextHK.getText().toString()).child(this.idUser).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                    }
                        Iterable<DataSnapshot> nodechilde1 = dataSnapshot.getChildren();
                    for (DataSnapshot childe : nodechilde1) {
                        // lay list id sinh vien
                        String id = childe.getKey().toString();
                        String subName = dataSnapshot.child(childe.getKey().toString()).child("TENMON").getValue().toString();
                        Subject_Class sc = new Subject_Class(subName,id);

                        sub_clasList.add(sc);

                        //    userIdList.add(childe.getKey());
                        //     Log.d("key1",childe.getValue().toString());

                    }

                    adapterSearchSubClass.notifyDataSetChanged();


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        } catch (Exception e)
        {
            Toast.makeText(getBaseContext(), "Error", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

}
