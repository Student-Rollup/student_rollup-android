package com.sourcey.appcaptone.Controller;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sourcey.appcaptone.R;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AttendenActivity extends AppCompatActivity implements View.OnTouchListener  {
    int PICK_IMAGE_REQUEST= 1;
    private Button btnDiemDanh,takeAPhoto;
    private Button btnKQ;
    private ImageView imgChupAnh;
    private EditText  Malop, Ngay, Thoigian ;
    // private Uri mimgurl;
    TextView idClass;
    private  String maLop;
    private static final  int PICK_IMAGE = 100;
    Uri imageURI;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    final List<String> userIdList = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    DatabaseReference myRef = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendancetoday);
        final  StorageReference storageRef = storage.getReference("diemdanh");
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String date_=dateFormat.format(date);
        MyFirebaseMessagingService my;
        String currentDateandTime = new SimpleDateFormat("HH:mm").format(new Date());

        idClass = (TextView) findViewById(R.id.txtIdClass);
        Ngay = (EditText)findViewById(R.id.editDate);
        Thoigian = (EditText) findViewById(R.id.editTime) ;

        Intent intent = getIntent();
        this.maLop = intent.getExtras().getString("MALOP");
        Ngay.setText(date_);
        Thoigian.setText(currentDateandTime);
        idClass.setText(intent.getExtras().getString("MALOP").toString());

        //
        btnKQ = (Button)findViewById(R.id.btnViewResult);
        btnDiemDanh=(Button) findViewById(R.id.btnSubmit);
        imgChupAnh=(ImageView) findViewById(R.id.imgAnh);
        takeAPhoto = (Button) findViewById(R.id.btnTakeAPhoto);
        imgChupAnh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallary();
            }
        });

        btnDiemDanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StorageReference mountainsRef = storageRef.child(idClass.getText().toString()).child(Ngay.getText().toString()).child(Thoigian.getText().toString());
                imgChupAnh.setDrawingCacheEnabled(true);
                imgChupAnh.buildDrawingCache();
                Bitmap bitmap = ((BitmapDrawable) imgChupAnh.getDrawable()).getBitmap();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] data = baos.toByteArray();
                UploadTask uploadTask = mountainsRef.putBytes(data);

                uploadTask.addOnFailureListener(new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                        Toast.makeText(AttendenActivity.this,"LOI!!!",Toast.LENGTH_SHORT).show();
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                        Uri downloadUrl =  taskSnapshot.getUploadSessionUri();
                        Toast.makeText(AttendenActivity.this,"THANH CONG!!!",Toast.LENGTH_SHORT).show();
                        // ...
                    }
                });
                setDefaultValue();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        senToRealTime();

                    }
                }, 15000);

            }
        });
        btnKQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent tam = new Intent(AttendenActivity.this, ViewResultAttenden.class);
              //  final Intent kq = new Intent(AttendenActivity.this,ViewResultAttenden.class);
                tam.putExtra("MALOP",maLop);
                startActivity(tam);
              //  startActivity(kq);

            }
        });

        takeAPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                 startActivityForResult(intent, PICK_IMAGE_REQUEST);
           }
// }
        });

    }

    public  void openGallary(){
        Intent gallary = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallary,PICK_IMAGE);

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d("anh","coanh");
//
////                 if(requestCode == RESULT_OK && requestCode == PICK_IMAGE){
////             imageURI = data.getData();
////             imgChupAnh.setImageURI(imageURI);
////
////         }
//                if(requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null){
//            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//            imgChupAnh.setImageBitmap(bitmap);
//                    Toast.makeText(AttendenActivity.this, "Chon Anh",
//                            Toast.LENGTH_SHORT).show();
//        }
//
//    }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       super.onActivityResult(requestCode, resultCode, data);

         if(requestCode == PICK_IMAGE && resultCode == RESULT_OK ){
             imageURI = data.getData();
             imgChupAnh.setImageURI(imageURI);

         }
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null){
//            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//           imgChupAnh.setImageBitmap(bitmap);
            imageURI = data.getData();
            imgChupAnh.setImageURI(imageURI);
       }
    }

    public  void show () {
        AlertDialog alertDialog = new AlertDialog.Builder(AttendenActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Alert message to be shown");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        alertDialog.show();
    }

    public  void setDefaultValue(){
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("Attendance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String tmp=dateFormat.format(date);
                Iterable<DataSnapshot> nodechilde = dataSnapshot.getChildren();
                for (DataSnapshot childe : nodechilde) {
                    // lay list id sinh vien

                    //  if (childe.child(tmp).getValue() != null)
//                    userIdList.add(childe.getKey());
////                      Log.d("key1",childe.getValue().toString());
//                    System.out.print("tren");
                    DatabaseReference myRef = database.getReference("DIEMDANHSINHVIEN_DIHOC");
                    myRef.child(idClass.getText().toString()).child("Attendance").child(childe.getKey()).child(Ngay.getText().toString()).child("KETQUA").setValue(
                            "0"
                    );
                }

//                for (final String s : userIdList) {
//                    //test1(s);
//                    System.out.print("duoi");
//
//                    Log.d("xxxx", s);
//                    laysv(s);
//                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Error
            }

        });
    }



    public void senToRealTime(){

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("DIEMDANHSINHVIEN_DIHOC");
        myRef.child(idClass.getText().toString()).child("Image").child(Ngay.getText().toString()).child(Thoigian.getText().toString()).setValue(
                "diemdanh/" + idClass.getText().toString() + "/" + Ngay.getText().toString() + "/" + Thoigian.getText().toString()
        );
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

}
