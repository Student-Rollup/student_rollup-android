package com.sourcey.appcaptone.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.Student_warn;
import com.sourcey.appcaptone.Model.UserDefault;
import com.sourcey.appcaptone.R;

import java.util.ArrayList;

public class ViewSituationMainActivity extends AppCompatActivity {
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    private DatabaseReference mDatabase;
    ArrayList<Student_warn> arrStudents = new ArrayList<>();
    public int tongNgayDiHoc = 5;
    Adapter_CanhBao adapter_canhBao;
    ListView listView;
    public String maLop;
    TextView txtClassID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_situation_main);

        final ProgressDialog progressDialog = new ProgressDialog(ViewSituationMainActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading !!!");
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 4000);
        listView = (ListView)findViewById(R.id.listViewSituation);
        txtClassID = (TextView)findViewById(R.id.txtClassID);
        Intent intent = getIntent();
        this.maLop = intent.getExtras().getString("MALOP");
        txtClassID.setText(maLop);
        getTinhHinhSinhVien();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               loadData();
            }
        }, 5000); // Millisecond 1000 = 1 sec
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),adapter_canhBao.getItem(position).getID(),Toast.LENGTH_LONG).show();
                String idSV = adapter_canhBao.getItem(position).getID();
                Intent truyen = new Intent(ViewSituationMainActivity.this, ViewSituationDetailMainActivity.class);
                truyen.putExtra("ID",idSV);
               truyen.putExtra("NAME",adapter_canhBao.getItem(position).getTEN());
                truyen.putExtra("URL",adapter_canhBao.getItem(position).getURL_HINHANH());
                truyen.putExtra("MALOP",maLop);

                //    truyen.putExtra("NAME","23232323");
             //   truyen.putExtra("URL","2323323232");
                startActivity(truyen);
            }
        });
    }


    void loadData() {


       // Log.d("us1",us.getIdUser());
        if(arrStudents.isEmpty()){
            Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();
        }
        adapter_canhBao = new Adapter_CanhBao(this, R.layout.custom_canhbao, arrStudents);
        listView.setAdapter(adapter_canhBao);
        listView.setDivider(new ColorDrawable(Color.parseColor("#E5E5E5")));
        listView.setDividerHeight(2);
    }
    void getTinhHinhSinhVien() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // mDatabase.child("THONGTINNGUOIDUNG").child("2121114121").addListenerForSingleValueEvent();
        //myRef.child("TAM1").setValue("TAM1");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // mDatabase.child("THONGTINNGUOIDUNG").child("2121114121").addListenerForSingleValueEvent();
        //myRef.child("TAM1").setValue("TAM1");
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(this.maLop).child("Attendance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<String> arr = new ArrayList<>();
                //arr.add(dataSnapshot.getChildren().toString());
                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    final String clubkey = childSnapshot.getKey();

                    myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("Attendance").child(clubkey).addValueEventListener(new ValueEventListener() {

                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getChildrenCount() == 0) {
                                Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                            }
                            final int size = (int) dataSnapshot.getChildrenCount();
                            myRef.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(clubkey).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String ten = dataSnapshot.child("TEN").getValue().toString();
                                    String url = dataSnapshot.child("URL_HINHANH").getValue().toString();

                                    String dem = "";
                                    if (size < tongNgayDiHoc - 2 ) {
                                        dem = "1";
                                    } else {
                                        dem = "0";
                                    }
                                        Student_warn st = new Student_warn(ten,dem,clubkey,url);
                                    arrStudents.add(st);
                                    /* Log.d("KQ",ten);*/

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }


                            });
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
//https://android--code.blogspot.com/2015/08/android-listview-alternate-row-color.html