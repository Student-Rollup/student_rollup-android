package com.sourcey.appcaptone.Controller;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.Student;
import com.sourcey.appcaptone.Model.Student_History;
import com.sourcey.appcaptone.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ViewResultAttenden extends AppCompatActivity
        //implements ValueEventListener
{
    final List<String> userIdList = new ArrayList();
    List<Student> listsv;
    ArrayList<Student_History> arrStudents = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabase;
    DatabaseReference myRef = database.getReference();
    EditText edtdata;
    TextView txtTong,txtIDClass,txtCoMat;
    AdapterSV adapterlop;
    ListView listView;
    Spinner loc;

    public  String sll;
    public  String maLop;
    TextView soluong ;
    // List<Class> loplist;
    //    FirebaseDatabase firebaseDatabase;
//    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kqdiemdanh);
        listView = (ListView) findViewById(R.id.listdata);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String tmp = dateFormat.format(date);
    //        soluong = (TextView)findViewById(R.id.to);
        edtdata = (EditText) findViewById(R.id.editDate);
        txtIDClass = (TextView) findViewById(R.id.txtIdClass);
        txtCoMat = (TextView) findViewById(R.id.editCoMat);
        txtTong = (TextView) findViewById(R.id.editTong);
        loc = (Spinner)findViewById(R.id.locKetQua);
        Intent intent = getIntent();
        this.maLop = intent.getExtras().getString("MALOP");
        edtdata.setText(tmp);
        txtCoMat.setText("12");
        txtTong.setText("29");
        txtIDClass.setText(maLop);
        ///  edtdata.setText(tmp);
     //   edtclass = (EditText) findViewById(R.id.edtclass);
       // edtclass.setText("MACLENIN1_BIS2");
        //  edtdata.setEnabled(false);
//        edtclass.setEnabled(false);

//         firebaseDatabase = FirebaseDatabase.getInstance();
//         databaseReference=firebaseDatabase.getReference().child("2018-2019").child("HK1").child("212121");
//          loplist = new ArrayList<>();
        addCombo();
        getKetQuaDiemDanh();

        adapterlop = new AdapterSV(this, R.layout.custom_history, arrStudents);
        listView.setAdapter(adapterlop);
        listView.setDivider(new ColorDrawable(Color.parseColor("#E5E5E5")));
        listView.setDividerHeight(2);
        new android.os.Handler().postDelayed(
                new Runnable() {

                    public void run() {
                        //soluong.setText(String.valueOf(arrStudents.size()));
                    }
                }, 7000);
        //databaseReference.addValueEventListener(this);
        //  databaseRef.addValueEventListener(this);
    }


    // @Override
//    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//
//
////        Iterable<DataSnapshot> nodechilde =dataSnapshot.getChildren();
////        for(DataSnapshot childe:nodechilde){
////            Class lhoc1 = childe.getValue(Class.class);
////            String tenlop = childe.getKey();
//////            Log.d("ttt",l);
////            lhoc1.setTENMON(tenlop);
////            loplist.add(lhoc1);
////            adapterlop.notifyDataSetChanged();
////        }
//
//    }

//    @Override
//    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//    }

    void laysv(final String ab) {
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(ab).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> nodechilde = dataSnapshot.getChildren();


                String CHUCVU = (String) dataSnapshot.child("CHUCVU").getValue();
                String EMAIL = (String) dataSnapshot.child("EMAIL").getValue();
                String QUYEN = (String) dataSnapshot.child("QUYEN").getValue();
                String TEN = (String) dataSnapshot.child("TEN").getValue();
                String URL_HINHANH = (String) dataSnapshot.child("URL_HINHANH").getValue();
                Student_History sv = new Student_History(URL_HINHANH, TEN,ab);
                arrStudents.add(sv);

                adapterlop.notifyDataSetChanged();
//                for ( final Student_History dd : arrStudents){
//                    Log.d("sv", dd.toString());
//                    arrStudents.removeAll(arrStudents);
//
//                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    void addCombo(){
        ArrayList<String> arr = new ArrayList<>();
        arr.add("Attendace");
        arr.add("Absent");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, arr);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loc.setAdapter(adapter);
    }
    public  void getKetQuaDiemDanh(){
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("Attendance").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String tmp=dateFormat.format(date);
                Iterable<DataSnapshot> nodechilde = dataSnapshot.getChildren();
                for (DataSnapshot childe : nodechilde) {
                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                    }
                    // lay list id sinh vien
                    //  if (childe.child(tmp).getValue() != null)
                    try {
                        String kq = dataSnapshot.child(childe.getKey()).child(tmp).child("KETQUA").getValue().toString();
                        Log.d("KQQ",kq);


                        if(kq.equals("1")==true) {
                            userIdList.add(childe.getKey());
                        }
                    }
                    catch (Exception e) {
                        Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                    }
//                      Log.d("key1",childe.getValue().toString());

                }

                for (final String s : userIdList) {
                    //test1(s);
                    Log.d("xxxx", s);
                    laysv(s);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Error
            }

        });
    }

    public void aaa() { // hàm lấy ID USer

        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(maLop).child("KETQUATHEONGAY").child("10-03-2019").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
               Date date = new Date();
                String tmp=dateFormat.format(date);
                Iterable<DataSnapshot> nodechilde = dataSnapshot.getChildren();
                for (DataSnapshot childe : nodechilde) {
                    // lay list id sinh vien
                  //  if (childe.child(tmp).getValue() != null)
                        userIdList.add(childe.getKey());
//                      Log.d("key1",childe.getValue().toString());
                    System.out.print("tren");

                }

                for (final String s : userIdList) {
                    //test1(s);
                    System.out.print("duoi");

                    Log.d("xxxx", s);
                    laysv(s);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Error
            }

        });
    }


}
