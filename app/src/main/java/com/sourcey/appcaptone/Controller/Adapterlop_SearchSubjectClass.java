package com.sourcey.appcaptone.Controller;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sourcey.appcaptone.Model.Subject_Class;
import com.sourcey.appcaptone.R;

import org.w3c.dom.Text;

import java.util.List;
public class Adapterlop_SearchSubjectClass extends ArrayAdapter<Subject_Class> {
    Context context;
    int resource;
    // List<Class> objects;

    List<Subject_Class> objects;
    public Adapterlop_SearchSubjectClass(@NonNull Context context, int resource, @NonNull List<Subject_Class> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
    private class ViewHolder{

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(resource,parent,false);
//        if(position %2 == 1)
//        {
//            // Set a background color for ListView regular row/item
//            view.setBackgroundColor(Color.parseColor("#AAAAAA"));
//
//        }
//        else
//        {
//
//        }
        TextView ten= (TextView) view.findViewById(R.id.txtSubName);
        TextView id = (TextView) view.findViewById(R.id.txtClassID);
        Subject_Class obj = objects.get(position);
        //  Class lop = objects.get(position);
        String sv =objects.get(position).toString();
        id.setText(obj.getClass_ID().toString());
        ten.setText(obj.getSub_Name().toString());
        return view;

    }



}
