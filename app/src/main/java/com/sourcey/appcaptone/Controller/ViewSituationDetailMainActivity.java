package com.sourcey.appcaptone.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sourcey.appcaptone.Model.Student_warn;
import com.sourcey.appcaptone.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewSituationDetailMainActivity extends AppCompatActivity {
    private DatabaseReference mDatabase;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    public  String idUser,name,url;
    TextView txtID,txtName,txtTime;
    ImageView avata;
    AdapterViewDetailSituation adapterViewDetailSituation;
    ArrayList<String> arrayList;
    ArrayList<String> arrayList2;

    ListView listView;
    String malop;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_situation_detail_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        listView = (ListView)findViewById(R.id.listviewDetail);
        txtTime = (TextView)findViewById(R.id.txtSLBuoiHoc);
       txtID = (TextView)findViewById(R.id.userID);
        txtName = (TextView)findViewById(R.id.userName);
        avata = (ImageView) findViewById(R.id.anh);
        setSupportActionBar(toolbar);
        Intent intent1 = getIntent();
       idUser = intent1.getExtras().getString("ID").toString();
        name = intent1.getExtras().getString("NAME").toString();
        url = intent1.getExtras().getString("URL").toString();
        malop =  intent1.getExtras().getString("MALOP").toString();
        Picasso.with(ViewSituationDetailMainActivity.this).load(url).into(avata);
        getData();
        Log.d("ANH",url);
        arrayList2 = new ArrayList<>();
        arrayList2.add("11"); arrayList2.add("22");
        final ProgressDialog progressDialog = new ProgressDialog(ViewSituationDetailMainActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading !!!");
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed

                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 2000);
       txtID.setText(idUser);
        txtName.setText(name);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txtTime.setText(String.valueOf(arrayList.size()));
                loadData();
            }
        },5000);



    }

    public  void getData(){
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // mDatabase.child("THONGTINNGUOIDUNG").child("2121114121").addListenerForSingleValueEvent();
        //myRef.child("TAM1").setValue("TAM1");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        // mDatabase.child("THONGTINNGUOIDUNG").child("2121114121").addListenerForSingleValueEvent();
        //myRef.child("TAM1").setValue("TAM1");
        myRef.child("DIEMDANHSINHVIEN_DIHOC").child(this.malop).child("Attendance").child(idUser).addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0) {
                    Toast.makeText(getBaseContext(), "No Data!", Toast.LENGTH_LONG).show();

                }
                ArrayList<String> arr = new ArrayList<>();
                arrayList = new ArrayList<>();
                //arr.add(dataSnapshot.getChildren().toString());
                for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    final String clubkey = childSnapshot.getKey();
                    arrayList.add(clubkey);
                    //arrayList.add("ssssss");
                    //arrayList.add("vvvvvv");
                    Log.d("DATA", String.valueOf(arrayList.size()));
                }

                }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    void loadData() {
        adapterViewDetailSituation = new AdapterViewDetailSituation(this, R.layout.customviewdetail, arrayList);
        listView.setAdapter(adapterViewDetailSituation);
        listView.setDivider(new ColorDrawable(Color.parseColor("#E5E5E5")));
        listView.setDividerHeight(2);

    }



}
