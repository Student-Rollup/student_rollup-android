package com.sourcey.appcaptone.Controller;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sourcey.appcaptone.Model.Student_warn;
import com.sourcey.appcaptone.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_CanhBao extends ArrayAdapter<Student_warn> {
    Context context;
    int resource;
    // List<Class> objects;

    List<Student_warn> objects;
    public Adapter_CanhBao(@NonNull Context context, int resource, @NonNull List<Student_warn> objects) {
        super(context, resource, objects);
        this.context=context;
        this.resource=resource;
        this.objects=objects;
    }
    private class ViewHolder{

    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(resource,parent,false);
       // ImageView anhCanhBap = (ImageView) view.findViewById(R.id.anhCanhBao);
        ImageView anh =(ImageView) view.findViewById(R.id.imageViewCanhBao) ;
        TextView ten= (TextView) view.findViewById(R.id.tenCanhBao);
        TextView id= (TextView) view.findViewById(R.id.idSVCanhBao);

        Student_warn sv =objects.get(position);
//        String url = sv.getURL_HINHANH();
//        Picasso.with(context).load(url).into(anh);
        id.setText(sv.getID());
        ten.setText(sv.getTEN());
        if (sv.getLv().equals("0")){
            anh.setVisibility(View.INVISIBLE);
        }
        Log.d("taaa", sv.getTEN()+"/"+sv.getLv());
        return view;

    }



}

