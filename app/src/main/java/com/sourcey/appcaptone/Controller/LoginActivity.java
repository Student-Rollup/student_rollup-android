package com.sourcey.appcaptone.Controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;
import com.sourcey.appcaptone.R;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnTouchListener {
    public  String name;
    public  String url;
    public  String chuVu;
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference();
    @BindView(R.id.input_email) EditText _emailText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login) Button _loginButton;
    private FirebaseAuth mAuth;
    public Boolean check = false;
    private DatabaseReference mDatabase;
    OkHttpClient mClient = new OkHttpClient();

// ...

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        String refreshedToken = "eWZq83ZIr3I:APA91bF7X0C7WL5wWoUdti0NYxllJn7Q-rEE8ur4caEBHWurIdmzZpJqxN85m9rB7sFLMtl5lCUV_GexnPSG_FCNiggxUhOZFDQ_EE8AUjiz94tus5fsyQx6_jYfnjIH9thYEZsUQdAP";//add your user refresh tokens who are logged in with firebase.

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(refreshedToken);
        _emailText.setText("212121");
        _passwordText.setText("123123");
        _emailText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                }
            }
        });



        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getEmail();
               // sendMessage(jsonArray,"Destroying surface Surface(name=com.sourcey.appcaptone/com.sourcey.appcaptone.Controller.Main2Activity)","How r u","Http:\\google.com","My Name is Vishal");

            }

        });

//        _signupLink.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // Start the Signup activity
//                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
//                startActivityForResult(intent, REQUEST_SIGNUP);
//                finish();
//                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
//            }
//        });
    }



    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }
    public void login() {
//        Log.d(TAG, "Login");
//
//        if (!validate()) {
//            onLoginFailed();
//            return;
//        }
//
//        _loginButton.setEnabled(false);
//
//        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
//                R.style.AppTheme_Dark_Dialog);
//        progressDialog.setIndeterminate(true);
//        progressDialog.setMessage("Authenticating...");
//        progressDialog.show();
//
//        String email = _emailText.getText().toString();
//        String password = _passwordText.getText().toString();
//
//        // TODO: Implement your own authentication logic here.
//
//        new android.os.Handler().postDelayed(
//                new Runnable() {
//                    public void run() {
//                        // On complete call either onLoginSuccess or onLoginFailed
//                        onLoginSuccess();
//                        // onLoginFailed();
//                        progressDialog.dismiss();
//                    }
//                }, 3000);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        if (validate() == true ){
                            myRef.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(_emailText.getText().toString()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.getChildrenCount() == 0){

                                    }
                                    else {
                                        name = dataSnapshot.child("TEN").getValue().toString();
                                        chuVu = dataSnapshot.child("CHUCVU").getValue().toString();
                                        url = dataSnapshot.child("URL_HINHANH").getValue().toString();
                                        finish();
                        Intent truyen = new Intent(LoginActivity.this, Main2Activity.class);
                        truyen.putExtra("URL",url);
                        truyen.putExtra("ID",_emailText.getText().toString());
                        truyen.putExtra("NAME",name);
                        truyen.putExtra("CHUCVU",chuVu);

                        startActivity(truyen);
                        getToken();

                        // login(email, _passwordText.getText().toString());
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }


            });

//            _loginButton.setEnabled(true);

        }
        else {
            return;
        }
    }

    public  void sendMess(){
//        String registrationToken = "eWZq83ZIr3I:APA91bF7X0C7WL5wWoUdti0NYxllJn7Q-rEE8ur4caEBHWurIdmzZpJqxN85m9rB7sFLMtl5lCUV_GexnPSG_FCNiggxUhOZFDQ_EE8AUjiz94tus5fsyQx6_jYfnjIH9thYEZsUQdAP";
//
//// See documentation on defining a message payload.
//        Message message = Message.buider()
//                .putData("score", "850")
//                .putData("time", "2:45")
//                .setToken(registrationToken)
//                .build();
//
//// Send a message to the device corresponding to the provided
//// registration token.
//        String response = FirebaseMessaging.getInstance().send(message);
//// Response is a message ID string.
//        System.out.println("Successfully sent message: " + response);
//        String registrationToken = "eWZq83ZIr3I:APA91bF7X0C7WL5wWoUdti0NYxllJn7Q-rEE8ur4caEBHWurIdmzZpJqxN85m9rB7sFLMtl5lCUV_GexnPSG_FCNiggxUhOZFDQ_EE8AUjiz94tus5fsyQx6_jYfnjIH9thYEZsUQdAP";
//
//        RemoteMessage message1 = new RemoteMessage.Builder(registrationToken)
//                .setMessageId("1")
//                .addData("message", "Hello")
//                .build();
//        FirebaseMessaging.getInstance().send(message1);
        //String response = FirebaseMessaging.getInstance().send(message1);
        OkHttpClient mClient = new OkHttpClient();


    }
    public void sendMessage(final JSONArray recipients, final String title, final String body, final String icon, final String message) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", body);
                    notification.put("title", title);
                    notification.put("icon", icon);

                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("registration_ids", recipients);

                    String result = postToFCM(root.toString());
                    Log.d("Main", "Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    Toast.makeText(LoginActivity.this, "Message Success: " + success + "Message Failed: " + failure, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "Message Failed, Unknown error occurred.", Toast.LENGTH_LONG).show();
                }
            }
        }.execute();
    }

    public String postToFCM(String bodyString) throws IOException {


        final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
        final MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.create(JSON, bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=" + "AAAAxrl6_K0:APA91bHpWAy-DF06jF27i-wdFF6BfxRxM1-gOjdTHTZvDSIiH69qe2wT6lToTumEztiyWPh504Xuj6-1Tp2j9YVC6yWV9Dku2ymzERtgrvzuK2pJZ3Smm_7YIKKrdoB6-mcnpP-NEvx_O5XBeitbFFfQ9rail5cKnw")
                .build();
        Response response = mClient.newCall(request).execute();
        return response.body().string();
    }

    ///https://firebase.googleblog.com/2016/08/sending-notifications-between-android.html
    //https://www.youtube.com/watch?v=NIGUpxJloj8
    public void  getToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        sendMess();
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        mDatabase.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(_emailText.getText().toString()).child("Token2").setValue(token);

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("Token", msg);
                     //   Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

              String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || password.isEmpty()) {
            if(email.isEmpty()){
                _emailText.setError("This field is require!");

            }
            if(password.isEmpty()){
                _passwordText.setError("This field is require!");

            }
            valid = false;
        }

        if ( password.length() < 4 || password.length() > 15) {
            _passwordText.setError("Password is between 4 and 10 characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    public  void getEmail() {
        if (validate() == true) {
            myRef.child("DIEMDANHSINHVIEN_DIHOC").child("THONGTINNGUOIDUNG").child(_emailText.getText().toString()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() == 0) {
//                    Toast.makeText(LoginActivity.this, "Lay Data k dc",
//                            Toast.LENGTH_SHORT).show();
                    } else {
                        String email = dataSnapshot.child("EMAIL").getValue().toString();
                        // login(email, _passwordText.getText().toString());
                        loginSystem(email);

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }


            });
        }

        else {

        }


    }
    public void loginSystem(String email) {
        mAuth.signInWithEmailAndPassword(email, _passwordText.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@androidx.annotation.NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Authentication Successfuly!",
                                    Toast.LENGTH_SHORT).show();
                            onLoginSuccess();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
      //  Toast.makeText(LoginActivity.this, "Login Successful!.",
       //                             Toast.LENGTH_SHORT).show();

//                          //  finish();
//        mAuth.signInWithEmailAndPassword("tam123@gmail.com", "123123")
//                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
//
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            // Sign in success, update UI with the signed-in user's information
//                            Toast.makeText(LoginActivity.this, "Login Successfuly!",
//                                    Toast.LENGTH_SHORT).show();
////                            Intent truyen = new Intent(LoginActivity.this, Main2Activity.class);
////                            truyen.putExtra("ID","212121");
////                            startActivity(truyen);
//
//                         //   Intent truyen = new Intent(LoginActivity.this, Main2Activity.class);
//                          //  truyen.putExtra("ID",_emailText.getText().toString());
//                          //  startActivity(truyen);
//                          //  finish();
//                          //  login2();
//                            //   _loginButton.setEnabled(false);
//                            // finish();
//                            //  onLoginSuccess();
////
////                  Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
////                startActivity(intent);
//                            // finish();
//                            // _loginButton.setEnabled(true);
//                            //   finish();
//                            // FirebaseUser user = mAuth.getCurrentUser();
//                        } else {
//                            // If sign in fails, display a message to the user.
//                            Toast.makeText(LoginActivity.this, "Login Failt!.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }
}
