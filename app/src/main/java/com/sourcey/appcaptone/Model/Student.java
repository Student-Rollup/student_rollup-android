package com.sourcey.appcaptone.Model;

public class Student {
    private  String CHUCVU,QUYEN,TEN,URL_HINHANH,EMAIL;

    public String getCHUCVU() {
        return CHUCVU;
    }

    public void setCHUCVU(String CHUCVU) {
        this.CHUCVU = CHUCVU;
    }

    public String getQUYEN() {
        return QUYEN;
    }

    public void setQUYEN(String QUYEN) {
        this.QUYEN = QUYEN;
    }

    public String getTEN() {
        return TEN;
    }

    public void setTEN(String TEN) {
        this.TEN = TEN;
    }

    public String getURL_HINHANH() {
        return URL_HINHANH;
    }

    public void setURL_HINHANH(String URL_HINHANH) {
        this.URL_HINHANH = URL_HINHANH;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public Student(){}

    public Student(String CHUCVU, String EMAIL, String QUYEN, String TEN, String URL_HINHANH){
        this.CHUCVU=CHUCVU;
        this.EMAIL =EMAIL;
        this.QUYEN=QUYEN;
        this.TEN=TEN;
        this.URL_HINHANH=URL_HINHANH;
    }

    public Student(String name , String URL)
    {
        this.TEN= name; this.URL_HINHANH = URL;
    }

}
