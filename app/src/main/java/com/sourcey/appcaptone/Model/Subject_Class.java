package com.sourcey.appcaptone.Model;

public class Subject_Class {

    public String getSub_Name() {
        return sub_Name;
    }

    public void setSub_Name(String sub_Name) {
        this.sub_Name = sub_Name;
    }

    public String getClass_ID() {
        return class_ID;
    }

    public void setClass_ID(String class_ID) {
        this.class_ID = class_ID;
    }

    public Subject_Class(String sub_Name, String class_ID) {
        this.sub_Name = sub_Name;
        this.class_ID = class_ID;
    }

    public   String sub_Name, class_ID;

}
