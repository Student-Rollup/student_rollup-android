package com.sourcey.appcaptone.Model;

public class SessionID {
    public SessionID(String id) {
        this.id = id;
    }

    public SessionID(String id, String name, String quyen) {
        this.id = id;
        this.name = name;
        Quyen = quyen;
    }

    public String id ;

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuyen() {
        return Quyen;
    }

    public void setQuyen(String quyen) {
        Quyen = quyen;
    }

    public String name;
    public  String Quyen;

    public String getId() {
        return id;
    }
}
